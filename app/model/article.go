package model

import (
	"github.com/jinzhu/gorm"
	"goblog/common"
	"goblog/config"
	"goblog/database"
	"time"
)

type Article struct {
	Id         int
	Title      string
	Name       string
	Image      string
	Content    string
	Click      int
	CreateTime time.Time
	ShowTime   string
	CommentCount int
}

func GetIndexArticle() []*Article {
	list := make([]*Article, 0)
	query := database.Ins.Order("create_time desc").Limit("10").Find(&list)
	if query.Error != nil {
		panic(query.Error)
	}
	for _, v := range list {
		v.ShowTime = common.TimeFormatMonth(v.CreateTime)
	}
	return list
}

func GetDetailArticle(id string) []*Article {
	row := make([]*Article, 0)
	query := database.Ins.
		Joins("left join "+config.Conf.Mysql.Prefix+"category on "+config.Conf.Mysql.Prefix+"article.category_id = "+config.Conf.Mysql.Prefix+"category.id").
		Select(config.Conf.Mysql.Prefix+"article.id,comment_count,title,name,go_article.image,go_article.content,click,"+config.Conf.Mysql.Prefix+"article.create_time").
		Take(&row, id)
	if query.Error != nil {
		panic(query.Error)
	}
	if err := database.Ins.
		Model(&Article{}).
		Where("id = ?", id).
		Update("click", gorm.Expr("click + ?", 1)).Error; err != nil {
		panic(err)
	}
	return row
}

func GetListArticle(title string, currentPage int) []*Article {
	list := make([]*Article, 0)
	if title != "" {
		database.Ins.
			Where("title like ?", "%"+title+"%").
			Order("weight desc,create_time desc").
			Find(&list)
	} else {
		//计算开始
		start := (currentPage - 1) * 10
		database.Ins.
			Order("weight desc,create_time desc").
			Offset(start).
			Limit(10).
			Find(&list)
	}
	for _, v := range list {
		v.ShowTime = common.TimeFormatMonth(v.CreateTime)
	}
	return list
}

func GetArticleCount() int {
	var count int
	query := database.Ins.Model(&Article{}).Count(&count)
	if query.Error != nil {
		panic(query.Error)
	}
	return count
}
