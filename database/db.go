package database

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"goblog/config"
)

var Ins *gorm.DB

var err error

func init() {

	dsn := fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=%s&parseTime=%s&loc=%s",
		config.Conf.Mysql.User,
		config.Conf.Mysql.Password,
		config.Conf.Mysql.Host,
		config.Conf.Mysql.Port,
		config.Conf.Mysql.DbName,
		config.Conf.Mysql.CharSet,
		config.Conf.Mysql.ParseTime,
		config.Conf.Mysql.Loc,
	)

	Ins, err = gorm.Open("mysql", dsn)

	if err != nil {
		panic(err)
	}

	Ins.SingularTable(config.Conf.Mysql.Enable)

	//设置表前缀
	gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
		return config.Conf.Mysql.Prefix + defaultTableName
	}
	//打印sql日志
	Ins.LogMode(config.Conf.Mysql.LogMode)
}
