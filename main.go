package main

import (
	"github.com/fvbock/endless"
	"goblog/route"
	"log"
)


func main() {
	if err := endless.ListenAndServe(":8080",route.Routes());err != nil{
		log.Fatalf("listen: %s\n", err)
	}
}
